'use strict';

var fs = require('fs');
var path = require('path');
var webpack = require('webpack');

var config = {

    resolve: {
        modules: [
            './node_modules','/usr/share/nodejs','/usr/lib/nodejs'
        ],
        extensions: [".tsx", ".ts", ".js", ".json"]
    },

    resolveLoader: {
        modules: ['./node_modules','/usr/share/nodejs','/usr/lib/nodejs'],
    },

    node: {
        global: true
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /(node_modules|bower_components|debian)/,
                use: {
                    // although we run our source through babel already, we need
                    // this for the dependencies
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env','@babel/preset-typescript'],
                    }
                }
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components|debian)/,
                use: {
                    // although we run our source through babel already, we need
                    // this for the dependencies
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-typescript'],
                    }
                }
            }
        ]
    },

    devtool: 'source-map',
};

module.exports = config;
